#! /bin/bash
printf '\e[8;50;120t'

while getopts v: option
do
case "${option}"
in
v) VERSION=${OPTARG};;
esac
done
if [[ $CI != TRUE ]]; then
cat << "EOF"
    ____              __  __                        ____                  __       ____        __       __             
   / __ \____ _____  / /_/ /_  ___  ____  ____     / __ \_______  _______/ /_     / __ \____ _/ /______/ /_  ___  _____
  / /_/ / __ `/ __ \/ __/ __ \/ _ \/ __ \/ __ \   / / / / ___/ / / / ___/ __ \   / /_/ / __ `/ __/ ___/ __ \/ _ \/ ___/
 / ____/ /_/ / / / / /_/ / / /  __/ /_/ / / / /  / /_/ / /  / /_/ (__  ) / / /  / ____/ /_/ / /_/ /__/ / / /  __/ /    
/_/    \__,_/_/ /_/\__/_/ /_/\___/\____/_/ /_/  /_____/_/   \__,_/____/_/ /_/  /_/    \__,_/\__/\___/_/ /_/\___/_/     
                                                                                                                       
EOF
fi

if [[ $CI != TRUE ]]; then
  echo Enter Drupal version to generate patch for:
  read D7_PATCH_VERSION

  VERSION=$D7_PATCH_VERSION 
fi

if [[ $VERSION == *"7"* ]]; then

  # if [[ $(echo "$VERSION > $D7_LATEST" |bc) -eq 1 ]]; then
  #   echo "Up to date!"
  #   exit 0
  # fi

  VERSION_SAFE=$(echo $VERSION | tr "." _)

  echo "Fetching version of Drupal 7"

  cd drops-7

  git fetch --all
  git checkout $VERSION

  if [[ $CI == TRUE ]]; then
    curl --request PUT --header "PRIVATE-TOKEN:$GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/10424488/variables/D7_PATCH_VERSION" --form "value=$VERSION"
    source ./export.bash
    D7_PATCH_VERSION=$D7_PATCH_VERSION
  fi
fi

if [[ $VERSION == *"8"* ]]; then

  VERSION=${D8_LATEST}
  VERSION_SAFE=$(echo $VERSION | tr "." _)

  wget https://updates.drupal.org/release-history/drupal/8.x

  echo "Fetching version of Drupal 8"

  cd drops-8

  git fetch --all
  git checkout $VERSION

  if [[ $CI == TRUE ]]; then
    curl --request PUT --header "PRIVATE-TOKEN:$GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/10424488/variables/D8_PATCH_VERSION" --form "value=$VERSION"
  fi

fi

cd ../drupal

git fetch --all
git checkout $VERSION

echo "Fetched"

cd ../

echo "Generating patch"

diff -rupN --exclude=".git" --exclude=".drush-lock-update" drupal/ drops-7/ > patches/drupal_$VERSION_SAFE.patch

echo Cleaning up patch

sed -i '' -e 's/drupal\///g' patches/drupal_$VERSION_SAFE.patch
sed -i '' -e 's/drops-7\///g' patches/drupal_$VERSION_SAFE.patch
sed -i '' -e 's/drops-8\///g' patches/drupal_$VERSION_SAFE.patch

echo 'Done find the patch in patches/drupal_'${VERSION_SAFE}'.patch'

